package assignment1_sd.a1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.TimeZone;

@SpringBootApplication
public class A1Application {

	public static void main(String[] args) {

		//TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		SpringApplication.run(A1Application.class, args);

	}

}
