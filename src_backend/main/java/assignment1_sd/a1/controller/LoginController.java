package assignment1_sd.a1.controller;

import assignment1_sd.a1.entities.User;
import assignment1_sd.a1.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class LoginController {
    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public int loginUser(@RequestBody User user) {return userService.login(user);}
}
