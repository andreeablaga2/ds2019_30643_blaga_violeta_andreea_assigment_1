package assignment1_sd.a1.controller;

import assignment1_sd.a1.entities.Medication;
import assignment1_sd.a1.repositories.MedicationRepository;
import assignment1_sd.a1.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/medication")
public class MedicationController {
    @Autowired
    private MedicationService medService;

    @PostMapping()
    public Integer createMed(@RequestBody Medication medication) {
        return medService.create(medication);
    }

    @GetMapping("/all")
    public List<Medication> retrieveAllMedication(){
        return medService.findAll();
    }

    @GetMapping("/{id}")
    public Medication retrieveMedicationByID(@PathVariable int id){
        return medService.findMedicationById(id);
    }

    @PutMapping("/{id}")
    public Medication updateMedication(@RequestBody Medication med, @PathVariable int id){
        return medService.update(med,id);
    }

    @DeleteMapping("/{id}")
    public List<Medication> deleteMed(@PathVariable int id) {
        return medService.delete(id);
    }
}
