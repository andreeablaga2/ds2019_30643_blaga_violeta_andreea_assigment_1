package assignment1_sd.a1.controller;

import assignment1_sd.a1.entities.Medication;
import assignment1_sd.a1.entities.MedicationPlan;
import assignment1_sd.a1.entities.Patient;
import assignment1_sd.a1.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/patients")
public class PatientController {
    @Autowired
    private PatientService patientService;

    @PostMapping
    public Integer insertPatient(@RequestBody Patient patient){
        return patientService.create(patient);
    }

    @GetMapping("/all")
    public List<Patient> retrievePatients(){
        return patientService.findAll();
    }

    @GetMapping("/{id}")
    public Patient retrievePatientByID(@PathVariable int id){
        return patientService.findPatientByID(id);
    }

    @PutMapping("/{id}")
    public Patient updatePatient(@RequestBody Patient patient, @PathVariable int id){
        return patientService.update(patient,id);
    }

    @DeleteMapping("/{id}")
    public List<Patient> deletePatient(@PathVariable int id){
        return patientService.delete(id);
    }

    @PostMapping("/{pID}/{medID}")
    public MedicationPlan createMedPlan(@PathVariable int pID, @PathVariable int medID, @RequestBody MedicationPlan medicationPlan){
        return patientService.addMedPlan(pID, medID, medicationPlan);
    }

    @GetMapping("/{id}/details")
    public Patient accountDetails(@PathVariable int id){
        return patientService.findPatientByID(id);
    }

    @GetMapping("/{id}/medplans")
    public List<MedicationPlan> viewMedPlans(@PathVariable int id){
        return patientService.viewMedPlans(id);
    }
}
