package assignment1_sd.a1.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="patients")
@PrimaryKeyJoinColumn(name="user_id")
public class Patient extends User {
//    @Id
//    @GeneratedValue(strategy= GenerationType.IDENTITY)
//    @Column(name = "patient_id")
//    private Integer patientID;

    @Column(name="medical_record")
    private String medicalRecord;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="caregiver_id", nullable=true)
    private Caregiver caregiver;

//    @OneToMany(fetch=FetchType.LAZY, mappedBy = "patient", cascade = CascadeType.ALL)
//    private List<MedicationPlan> medicationPlan = new ArrayList<>();

    public Patient() {
    }

    public Patient(String username, String password,String name, String birthDate, String gender, String address, String medicalRecord){
        super(username,password,name,birthDate,gender,address);
        this.medicalRecord = medicalRecord;
    }

    public Patient(Integer id, String username, String password, int type, String name, String birthDate, String gender, String address, String medicalRecord, Caregiver caregiver) {
        super(id,username,password,type,name,birthDate,gender,address);
        this.medicalRecord = medicalRecord;
        this.caregiver = caregiver;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

//    public Caregiver getCaregiver() {
//        return caregiver;
//    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }
}
