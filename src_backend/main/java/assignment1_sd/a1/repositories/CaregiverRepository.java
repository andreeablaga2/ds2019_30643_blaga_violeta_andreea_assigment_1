package assignment1_sd.a1.repositories;

import assignment1_sd.a1.entities.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CaregiverRepository extends JpaRepository<Caregiver, Integer> {
}
