package assignment1_sd.a1.services;

import assignment1_sd.a1.entities.Caregiver;
import assignment1_sd.a1.entities.Patient;
import assignment1_sd.a1.repositories.CaregiverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Console;
import java.util.List;
import java.util.Optional;

@Service
public class CaregiverService {
    @Autowired
    private CaregiverRepository caregiverRepository;

    public Integer create(Caregiver caregiver){
        Caregiver newc = caregiverRepository.save(caregiver);

        return newc.getUserID();
    }

    public List<Caregiver> findAll() {
        List<Caregiver> all = caregiverRepository.findAll();
        return all;
    }

    public Caregiver findCaregiverByID(int caregiverID) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverID);
        if(caregiver==null){
            System.out.println("Caregiver does not exist");
            return null;
        }

        Caregiver c = new Caregiver();
        c.setUserID(caregiver.get().getUserID());
        c.setUsername(caregiver.get().getUsername());
        c.setPassword(caregiver.get().getPassword());
        c.setType(caregiver.get().getType());
        c.setName(caregiver.get().getName());
        c.setBirthDate(caregiver.get().getBirthDate());
        c.setGender(caregiver.get().getGender());
        c.setAddress(caregiver.get().getAddress());
        c.setPatients(caregiver.get().getPatients());

        return c;

    }

    public Caregiver update(Caregiver caregiver, int caregiverID) {
        Optional<Caregiver> c = caregiverRepository.findById(caregiverID);
        if (c==null) {
            System.out.println("Caregiver does not exist");
            return null;
        }
        caregiver.setUserID(caregiverID);
        caregiverRepository.save(caregiver);
        return caregiver;
    }

    public List<Caregiver> delete(int caregiverID){
        caregiverRepository.deleteById(caregiverID);
        List<Caregiver> all = caregiverRepository.findAll();
        return all;
    }

    //view list of patients
    public List<Patient> viewPatients(int caregiverID){
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverID);
        if(caregiver==null){
            System.out.println("Caregiver does not exist");
            return null;
        }

        if(caregiver.get().getType() != 2){
            System.out.println("Only the caregiver is allowed to view its list of patients");
            return null;
        }

        Caregiver c = new Caregiver();
        c.setPatients(caregiver.get().getPatients());
        return c.getPatients();
    }
}
