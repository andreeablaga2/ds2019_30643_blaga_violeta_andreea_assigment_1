import React from 'react'
import logo from './commons/images/icon.png';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const NavigationBar = () => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"55"} />
            </NavbarBrand>
            <Nav className="mr-auto" navbar>

                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style={textStyle} nav caret>
                       Menu
                    </DropdownToggle>

                    <DropdownMenu right >

                        {/* <DropdownItem>
                            <NavLink href="/persons">Persons</NavLink>
                        </DropdownItem> */}

                        <DropdownItem>
                            <NavLink href="/patients">Patients</NavLink>
                        </DropdownItem>

                        <DropdownItem>
                            <NavLink href="/caregivers">Caregivers</NavLink>
                        </DropdownItem>

                        <DropdownItem>
                            <NavLink href="/medication">Medication</NavLink>
                        </DropdownItem>

                        <DropdownItem>
                            <NavLink href="/doctor">Doctor</NavLink>
                        </DropdownItem>

                    </DropdownMenu>
                </UncontrolledDropdown>

                <NavLink style= {{color:"white"}}href="/login">Login</NavLink>

            </Nav>
        </Navbar>
    </div>
);

export default NavigationBar
